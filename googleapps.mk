# Copyright (C) 2018 The PixelDust Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    FaceLock \
    GoogleCamera \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GoogleTTS \
    GoogleVrCore \
    LatinIMEGooglePrebuilt \
    NexusWallpapersStubPrebuilt2017 \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    talkback \
    Tycho \
    WallpapersBReel2017

# framework
PRODUCT_PACKAGES += \
    com.google.android.camera.experimental2016 \
    com.google.android.dialer.support \
    com.google.android.maps \
    com.google.android.media.effects \
    com.google.widevine.software.drm

# priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    ConfigUpdater \
    ConnMetrics \
    GCS \
    GmsCoreSetupPrebuilt \
    GoogleBackupTransport \
    GoogleContacts \
    GoogleDialer \
    GoogleExtServices \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    NexusLauncherPrebuilt \
    Phonesky \
    PrebuiltGmsCorePix \
    SetupWizard \
    StorageManagerGoogle \
    TagGoogle \
    Turbo \
    Velvet \
    WallpaperPickerGooglePrebuilt

# Libraries
PRODUCT_COPY_FILES += \
    vendor/googleapps/lib64/libgdx.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libgdx.so \
    vendor/googleapps/lib64/libwallpapers-breel-jni.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libwallpapers-breel-jni.so

# Symlinks
PRODUCT_PACKAGES += \
    libfacenet.so \
    libgdx.so \
    libwallpapers-breel-jni.so \
    libjpeg.so

# Blobs
PRODUCT_COPY_FILES += \
    vendor/googleapps/etc/permissions/com.google.android.camera.experimental2016.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.camera.experimental2016.xml \
    vendor/googleapps/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/googleapps/etc/permissions/com.google.android.maps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.maps.xml \
    vendor/googleapps/etc/permissions/com.google.android.media.effects.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.android.media.effects.xml \
    vendor/googleapps/etc/permissions/com.google.widevine.software.drm.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.google.widevine.software.drm.xml \
    vendor/googleapps/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default-permissions/default-permissions.xml \
    vendor/googleapps/etc/permissions/privapp-permissions-googleapps.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-googleapps.xml \
    vendor/googleapps/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/preferred-apps/google.xml \
    vendor/googleapps/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google.xml \
    vendor/googleapps/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_build.xml \
    vendor/googleapps/etc/sysconfig/google_vr_build.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/google_vr_build.xml \
    vendor/googleapps/etc/sysconfig/pixel_2017.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017.xml \
    vendor/googleapps/etc/sysconfig/pixel_2017_exclusive.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/pixel_2017_exclusive.xml \
    vendor/googleapps/lib/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfilterpack_facedetect.so \
    vendor/googleapps/lib/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libfrsdk.so \
    vendor/googleapps/lib64/libfacenet.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfacenet.so \
    vendor/googleapps/lib64/libfrsdk.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfrsdk.so \
    vendor/googleapps/lib64/libfilterpack_facedetect.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libfilterpack_facedetect.so \
    vendor/googleapps/usr/srec/en-US/am_phonemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/am_phonemes.syms \
    vendor/googleapps/usr/srec/en-US/app_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/app_bias.fst \
    vendor/googleapps/usr/srec/en-US/APP_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/APP_NAME.fst \
    vendor/googleapps/usr/srec/en-US/APP_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/APP_NAME.syms \
    vendor/googleapps/usr/srec/en-US/c_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/c_fst \
    vendor/googleapps/usr/srec/en-US/CLG.prewalk.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CLG.prewalk.fst \
    vendor/googleapps/usr/srec/en-US/commands.abnf:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/commands.abnf \
    vendor/googleapps/usr/srec/en-US/compile_grammar.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/compile_grammar.config \
    vendor/googleapps/usr/srec/en-US/config.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/config.pumpkin \
    vendor/googleapps/usr/srec/en-US/confirmation_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/confirmation_bias.fst \
    vendor/googleapps/usr/srec/en-US/CONTACT_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CONTACT_NAME.fst \
    vendor/googleapps/usr/srec/en-US/CONTACT_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/CONTACT_NAME.syms \
    vendor/googleapps/usr/srec/en-US/contacts.abnf:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts.abnf \
    vendor/googleapps/usr/srec/en-US/contacts_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts_bias.fst \
    vendor/googleapps/usr/srec/en-US/contacts_disambig.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/contacts_disambig.fst \
    vendor/googleapps/usr/srec/en-US/dict:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dict \
    vendor/googleapps/usr/srec/en-US/dictation.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dictation.config \
    vendor/googleapps/usr/srec/en-US/dnn:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/dnn \
    vendor/googleapps/usr/srec/en-US/embedded_class_denorm.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/embedded_class_denorm.mfar \
    vendor/googleapps/usr/srec/en-US/embedded_normalizer.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/embedded_normalizer.mfar \
    vendor/googleapps/usr/srec/en-US/endpointer_dictation.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_dictation.config \
    vendor/googleapps/usr/srec/en-US/endpointer_model:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_model \
    vendor/googleapps/usr/srec/en-US/endpointer_model.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_model.mmap \
    vendor/googleapps/usr/srec/en-US/endpointer_voicesearch.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/endpointer_voicesearch.config \
    vendor/googleapps/usr/srec/en-US/ep_portable_mean_stddev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/ep_portable_mean_stddev \
    vendor/googleapps/usr/srec/en-US/ep_portable_model.uint8.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/ep_portable_model.uint8.mmap \
    vendor/googleapps/usr/srec/en-US/g2p.data:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p.data \
    vendor/googleapps/usr/srec/en-US/g2p_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_fst \
    vendor/googleapps/usr/srec/en-US/g2p_graphemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_graphemes.syms \
    vendor/googleapps/usr/srec/en-US/g2p_phonemes.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/g2p_phonemes.syms \
    vendor/googleapps/usr/srec/en-US/grammar.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/grammar.config \
    vendor/googleapps/usr/srec/en-US/hmmlist:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hmmlist \
    vendor/googleapps/usr/srec/en-US/hmm_symbols:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/hmm_symbols \
    vendor/googleapps/usr/srec/en-US/input_mean_std_dev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/input_mean_std_dev \
    vendor/googleapps/usr/srec/en-US/lexicon.U.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/lexicon.U.fst \
    vendor/googleapps/usr/srec/en-US/lstm_model.uint8.data:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/lstm_model.uint8.data \
    vendor/googleapps/usr/srec/en-US/magic_mic.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/magic_mic.config \
    vendor/googleapps/usr/srec/en-US/media_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/media_bias.fst \
    vendor/googleapps/usr/srec/en-US/metadata:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/metadata \
    vendor/googleapps/usr/srec/en-US/monastery_config.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/monastery_config.pumpkin \
    vendor/googleapps/usr/srec/en-US/norm_fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/norm_fst \
    vendor/googleapps/usr/srec/en-US/offensive_word_normalizer.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/offensive_word_normalizer.mfar \
    vendor/googleapps/usr/srec/en-US/offline_action_data.pb:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/offline_action_data.pb \
    vendor/googleapps/usr/srec/en-US/phonelist:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/phonelist \
    vendor/googleapps/usr/srec/en-US/portable_lstm:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/portable_lstm \
    vendor/googleapps/usr/srec/en-US/portable_meanstddev:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/portable_meanstddev \
    vendor/googleapps/usr/srec/en-US/pumpkin.mmap:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/pumpkin.mmap \
    vendor/googleapps/usr/srec/en-US/read_items_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/read_items_bias.fst \
    vendor/googleapps/usr/srec/en-US/rescoring.fst.compact:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/rescoring.fst.compact \
    vendor/googleapps/usr/srec/en-US/semantics.pumpkin:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/semantics.pumpkin \
    vendor/googleapps/usr/srec/en-US/skip_items_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/skip_items_bias.fst \
    vendor/googleapps/usr/srec/en-US/SONG_NAME.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/SONG_NAME.fst \
    vendor/googleapps/usr/srec/en-US/SONG_NAME.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/SONG_NAME.syms \
    vendor/googleapps/usr/srec/en-US/time_bias.fst:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/time_bias.fst \
    vendor/googleapps/usr/srec/en-US/transform.mfar:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/transform.mfar \
    vendor/googleapps/usr/srec/en-US/voice_actions.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/voice_actions.config \
    vendor/googleapps/usr/srec/en-US/voice_actions_compiler.config:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/voice_actions_compiler.config \
    vendor/googleapps/usr/srec/en-US/word_confidence_classifier:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/word_confidence_classifier \
    vendor/googleapps/usr/srec/en-US/wordlist.syms:$(TARGET_COPY_OUT_SYSTEM)/usr/srec/en-US/wordlist.syms
